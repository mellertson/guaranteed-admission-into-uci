# Guarantee of Admission for Parker Ellertson into UCI

---

You can be **guaranteed** admission into UCI!  To learn more keep reading.

In short, you must submit a [TAG application][tag_application] correctly.  And must meet some 
minimum requirements.  Detailed information on both is provided in the following
sections.  And some additional resources are provided [here][assist], which are 
"intended to help students and counselors work together to establish an appropriate path 
toward transferring from a public California community college to a public 
California university."

## Summary of Requirements

---

Generally speaking, to be guaranteed admission into UCI a year before you attend 
you must do the following:

* Submit your [TAG application][tag_application] between September 1 - 30.
* Submit your [UC undergraduate application][uc_application] between October 1 - November 30.
* One of the following majors must be selected and cannot change after your TAG 
application is submitted:
    - [Computer Engineering, B.S.][ce_degree]
    - [Electrical Engineering, B.S.][ee_degree]
* Have a cumulative UC transferable GPA of 3.4.
* Earn a grade of C or better in all listed major
preparation courses while maintaining a cumulative GPA of 3.0 in some required courses.
  
**NOTE** the list of requirements above is a summary only and does not contain
all requirements.  A list of all requirements is provided on 
[UCI's web-site][tag_full_requirements] and section 
[Required Courses](#required-courses) below.

## TAG Application

---

The Transfer Admission Guarantee (TAG) is a program that offers students 
from a community college **guaranteed admission** to several colleges and 
universities. The writing of a TAG contract enables qualified students to be 
guaranteed admissions one year prior to transfer.

Because a TAG application is a contract, and contracts add a bit of complexity,
workshops are available to tecah you how to correctly submit your TAG 
application [here][tag_workshops].

## Required Courses

--- 

The following is list of all required courses, which must be completed with 
a C or better.

- Single Variable Calculus I (C-ID MATH 210 or MATH 211)
- Single Variable Calculus II (C-ID MATH 220 or MATH 221) or 2 semester/quarters of Single Variable Calculus Sequence (C-ID MATH 900S or
910S)
- Multivariable Calculus (C-ID MATH 230)
- Ordinary Differential Equations (C-ID MATH 240) or Differential Equations and Linear Algebra (C-ID MATH 910S)
- Introduction to Linear Algebra (C-ID MATH 250) or Differential Equations and Linear Algebra (C-ID MATH 910S)
- Calculus-Based Physics for Scientists and Engineers: A (C-ID PHYS 205)
- Calculus-Based Physics for Scientists and Engineers: B (C-ID PHYS 210)
- Calculus-Based Physics for Scientists and Engineers: C (C-ID PHYS 215)
or Calculus-Based Physics for Scientists and Engineers: ABC (C-ID PHYS 200S)
- Programming Concepts and Methodology I (C-ID comp 122) or Introduction to Programming Concepts and Methodologies for Engineers (C-ID
ENGR 120)
- Circuit Analysis (C-ID ENGR 260)



[tag_full_requirements]: https://admissions.uci.edu/apply/transfer-students/guaranteed-admissions.php
[tag_application]: https://uctap.universityofcalifornia.edu/
[uc_application]: https://admission.universityofcalifornia.edu/apply-now.html
[tag_workshops]: https://www.smc.edu/student-support/academic-support/transfer-center/transfer-admission-guarantee/#:~:text=Transfer%20Admission%20Guarantee%20(TAG)%20is,one%20year%20prior%20to%20transfer.
[ce_degree]: ./OCC_to_UCI_for_2021-2022_by_Computer_Engineering,_B.S..pdf
[ee_degree]: ./OCC_to_UCI_for_2021-2022_by_Electrical_Engineering,_B.S..pdf
[assist]: https://assist.org/
